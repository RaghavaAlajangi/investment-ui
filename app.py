import dash
import dash_bootstrap_components as dbc
import dash_bootstrap_templates as dbt
from dash import Input, Output, State, dcc, html
from components import wrong_page
from utils import url_theme1, dbc_css

app = dash.Dash(
    external_stylesheets=[url_theme1, dbc_css],
    # these meta_tags ensure content is scaled correctly on different devices
    # see: https://www.w3schools.com/css/css_rwd_viewport.asp for more
    meta_tags=[
        {"name": "viewport", "content": "width=device-width, initial-scale=1"}
    ])

server = app.server


def app_title_theme():
    component = dbc.Row([
        dbc.Col([
            html.H2("Stocks UI", className="text-white"),
            html.Br(),
            dbt.ThemeSwitchAIO(aio_id="theme",
                               themes=[url_theme1, url_theme1]),
            html.Br()
        ]),
    ])
    return component


def app_description():
    component = html.P(
        className="lead",
        children=["A short app description"])
    return component


def login_menu():
    component = dbc.Row([
        dbc.Col([
            html.Br(),
            dbc.DropdownMenu([
                dbc.DropdownMenuItem("Login", id="login-dropdown"),
                dbc.DropdownMenuItem("Signup", id="signup-dropdown"),
            ],
                label="Login / Signup",
                color="primary",
            ),
            html.Br(),
            html.Br(),
        ]),
    ])
    return component


def sidebar_menu():
    component = dbc.Nav([
        dbc.NavLink("Home", href="/", active="exact"),
        dbc.NavLink("Pipelines", href="/pipelines", active="exact"),
        dbc.NavLink("Page2", href="/page2", active="exact"),
        dbc.NavLink("Page3", href="/page3", active="exact"),
        dbc.NavLink("Page4", href="/page4", active="exact"),
    ],
        vertical=True,
        pills=True,
    )
    return component


def sidebar_section():
    component = html.Div(
        id="sidebar",
        children=[
            app_title_theme(),
            app_description(),
            login_menu(),
            sidebar_menu()
        ])
    return component


def content_block():
    component = html.Div(
        id="page-content",
        style={
            "align-items": "center"
        })
    return component


def pipeline_tabs():
    tab1_content = dbc.Card(
        dbc.CardBody(
            [
                html.P("This is tab 1!"),
                dbc.Button("Click here", color="success"),
            ]
        ),
        className="mt-3",
    )

    tab2_content = dbc.Card(
        dbc.CardBody(
            [
                html.P("This is tab 2!", className="card-text"),
                dbc.Button("Don't click here", color="danger"),
            ]
        ),
        className="mt-3",
    )

    tabs = dbc.Tabs(
        [
            dbc.Tab(tab1_content, label="Open requests",
                    active_label_style={"color": "#017b70"}),
            dbc.Tab(tab2_content, label="Closed requests",
                    active_label_style={"color": "#017b70"}),
        ]
    )
    home = [
        html.P("This page is responsible for running RTDC dataset "
               "processing pipelines on MPCDF gpu clusters (HPC)"),
        html.Br(),
        dbc.Button([
            dbc.NavLink("New request",
                        href="pipelines/opened-requests",
                        active="exact"),
        ],
            color="primary",
            className="my-button-class"
        ),
        tabs,

    ]
    return home


# Build the layout of the app
app.layout = html.Div([
    dcc.Location(id="url"),
    sidebar_section(),
    content_block()
])


@app.callback(Output("page-content", "children"),
              [Input("url", "pathname"),
               # Input(dbt.ThemeSwitchAIO.ids.switch("theme"), "value")
               ])
def render_page_content(pathname):
    # template = template_theme1 if toggle else template_theme2
    if pathname == "/":
        return html.P("Oh cool, this is page 2!")
    elif pathname == "/pipelines":
        return pipeline_tabs()
    elif pathname == "/page2":
        return html.P("Oh cool, this is page 2!")
    elif pathname == "/page3":
        return html.P("Oh cool, this is page 3!")
    elif pathname == "/page4":
        return html.P("Oh cool, this is page 4!")
    # Return a 404 message, if user tries to reach undefined page
    return wrong_page(pathname)


if __name__ == "__main__":
    app.run_server(port=1000, debug=True)
