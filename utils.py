import dash_bootstrap_components as dbc

url_theme1 = dbc.themes.SKETCHY
url_theme2 = dbc.themes.DARKLY

dbc_css = (
    "https://cdn.jsdelivr.net/gh/AnnMarieW/dash-bootstrap-templates@"
    "V1.0.1/dbc.min.css"
)
