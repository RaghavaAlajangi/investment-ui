<!-- pred_advanced -->

# Pipeline Request ADVANCED

Pipeline for segmentation and/or classification (prediction) and analysis of data.

**Note**: This is the advanced template. If you are not sure which template to use, then you should probably
use the other template (simple).

### How to use this template:

Tick the Steps in "Your Pipeline Details" you wish to execute.
See this video link [] for how to use this template.

Choosing multiple Segmentation or Prediction algorithms will create a matrix of
jobs (multiple jobs). You can change the keyword=value.



- To start the job type the following in a comment:
  
      Go

- To cancel the job, type the following in a comment:

      Cancel

Updates will be displayed as comments when the process starts and ends.
For example, when the job(s) is finished, a comment with "Finished" will appear.


## Your Pipeline Details

- **Segmentation**
  - dcevent version:
    - [x] dcevent version=latest
    <!-- option end -->
  - Segmentation Algorithm
    - [x] mlunet: UNET
    <!-- option end -->
    - [x] legacy: Legacy thresholding with OpenCV
      - [x] thresh=-6
      - [x] blur=0
      - [ ] binaryops=5
      - [x] diff_method=1
      - [x] clear_border=True
      - [x] fill_holes=True
      - [x] closing_disk=5
    <!-- option end -->
    - [x] watershed: Watershed algorithm
      - [x] clear_border=True
      - [x] fill_holes=True
      - [x] closing_disk=5
    <!-- option end -->
    - [ ] std: Standard-deviation-based thresholding
      - [ ] clear_border=True
      - [ ] fill_holes=True
      - [ ] closing_disk=5
    <!-- option end -->
  - Background Correction/Subtraction Method
    - [x] rollmed: Rolling median RT-DC background image computation
      - [x] kernel_size=100
      - [x] batch_size=10000
    <!-- option end -->
    - [x] sparsemed: Sparse median background correction with cleansing
      - [x] kernel_size=200
      - [x] split_time=1.0
      - [x] thresh_cleansing=0
      - [x] frac_cleansing=0.8
    <!-- option end -->
  - Available gating options:
    - [x] norm gating:
      - [ ] online_gates=False
      - [x] size_thresh_mask=5
    <!-- option end -->
  - Further Options:
    - [ ] --reproduce
    <!-- option end -->

- **Prediction**
   - Classification Model
     - [x] bloody-bunny_g1_bacae: Bloody Bunny
     <!-- option end -->

- **Post Analysis**
   - [x] Benchmarking
   - [x] Scatter plot
   <!-- option end -->


- **Data to Process**
   - [ ] DCOR: cfs
   - [ ] DCOR: control
   - [ ] DCOR: figshare-7771184-v2
   - [ ] DCOR: 89bf2177-ffeb-9893-83cc-b619fc2f6663
   - [x] DCOR: 7d4936b2-5203-4feb-a2ea-a9ce9a9d2edb
   - [ ] DVC: data_raw/test_data/calibration_beads.rtdc
   - [ ] All Data
   <!-- option end -->
