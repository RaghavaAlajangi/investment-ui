<!-- pred_simple -->

# Pipeline Request

Pipeline for segmentation and/or classification (prediction) and analysis of data.


### How to use this template:

Tick the Steps in "Your Pipeline Details" you wish to execute.
See this video link [] for how to use this template.

Choosing multiple Segmentation or Prediction algorithms will create a matrix of
jobs (multiple jobs).


- To start the job type the following in a comment:
  
      Go

- To cancel the job, type the following in a comment:

      Cancel

Updates will be displayed as comments when the process starts and ends.
For example, when the job(s) is finished, a comment with "Finished" will appear.

## Your Pipeline Details

- **Segmentation**
   - Segmentation Algorithm
      - [x] mlunet: UNET
      <!-- option end -->
      - [x] legacy: Legacy thresholding with OpenCV
      <!-- option end -->
      - [ ] watershed: Watershed algorithm
      <!-- option end -->
      - [x] std: Standard-deviation-based thresholding
      <!-- option end -->


- **Prediction**
   - Classification Model
      - [ ] mnet: MNet
      - [x] bloody-bunny_g1_bacae: Bloody Bunny


- **Post Analysis**
   - [ ] Benchmarking
   - [x] Scatter Plots


- **Data to Process**
   - [ ] DCOR: cfs
   - [ ] DCOR: control
   - [ ] DCOR: figshare-7771184-v2
   - [ ] DCOR: 89bf2177-ffeb-9893-83cc-b619fc2f6663
   - [x] DCOR: 7d4936b2-5203-4feb-a2ea-a9ce9a9d2edb
   - [ ] DVC: data_raw/test_data/calibration_beads.rtdc
   - [ ] All Data

